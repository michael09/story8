from django.test import TestCase
from .views import index
from django.test import Client
from django.urls import resolve

# Create your tests here.

class Lab8UnitTest(TestCase):

    def test_lab8_url_is_exist(self):
        response = Client().get('/lab8')
        self.assertEqual(response.status_code, 200)

    def test_lab8_landing_page_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_index_func(self):
        found = resolve('/lab8')
        self.assertEqual(found.func, index)

    def test_lab6_landing_page_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)