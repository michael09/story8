$(window).on('load', function() {
  $('#status').delay(3000).fadeOut();
  $('#preloader').delay(3500).fadeOut('slow');
  $('body').delay(3500).css({'overflow':'visible'});
});

$(document).ready(function(){
    $( function() {
    var icons = {
      header: "ui-icon-circle-arrow-e",
      activeHeader: "ui-icon-circle-arrow-s"
    };
    $( "#accordion" ).accordion({
      icons: icons, collapsible: true, active: false
    });
    $( "#toggle" ).button().on( "click", function() {
      if ( $( "#accordion" ).accordion( "option", "icons" ) ) {
        $( "#accordion" ).accordion( "option", "icons", null );
      } else {
        $( "#accordion" ).accordion( "option", "icons", icons );
      }
    });
  } );
});


$(document).ready(function () {
    themes = JSON.stringify([
        {"id": 0, "bodyColor": "gainsboro", "backC": "blanchedalmond", "navbar": "white"},
        {"id": 1, "bodyColor": "black", "backC": "gray", "navbar": "gray"}
    ]);
    localStorage.setItem("themes", themes);
    var id = localStorage.getItem("id");
    id = parseInt(id);
    if (Number.isNaN(id)) {
        changeTheme(0);
    } else {
        changeTheme(id);
    }

    $('.dark').on('click', function() {
        changeTheme(1);
        localStorage.setItem("id","1")
    });

    $('.light').on('click', function() {
        changeTheme(0);
        localStorage.setItem("id","0")
    })
});

function changeTheme(x) {
    var themeArray = JSON.parse(localStorage.getItem("themes"));
    var selectedTheme = themeArray[x];

    localStorage.setItem("selectedTheme", selectedTheme);
    $('body').css({
        "background-color": selectedTheme.bodyColor
    });
    $('.container').css({
        "background-color": selectedTheme.backC
    });
    $('.navbar').css({
        "background-color": selectedTheme.navbar
    });
}
